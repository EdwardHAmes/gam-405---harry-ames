﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class BaseAI : ScriptableObject
{
    public enum CombatantAction
    {
        Move,
        DropBomb
    }

    // Get the action the combatant wants to take
    // If you are returning CombatantAction.Move:
    // - You must specify how you would like to move by adding data to the aMoves list
    // - You will move along the path until you bump into something or reach the end
    // If you are returning CombatantAction.DropBomb:
    // - You must specify what the timer on the bomb should be set by setting the aBombTime argument
    public abstract CombatantAction GetAction(ref List<GameManager.Direction> aMoves, ref int
    aBombTime);


}
