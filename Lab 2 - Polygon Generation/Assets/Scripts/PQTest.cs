﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PQTest : MonoBehaviour
{
    PriorityQueue<string> names = new PriorityQueue<string>();
    // Start is called before the first frame update
    void Start()
    {
        string[] nameArray = new string[10];

        nameArray[0] = "Haniya Mullins";
        nameArray[1] = "Kamron Burt";
        nameArray[2] = "Jordan Mayo";
        nameArray[3] = "Phyllis Smith";
        nameArray[4] = "Isaiah Harper";
        nameArray[5] = "Madeline Thatcher";
        nameArray[6] = "Jonah Ratcliffe";
        nameArray[7] = "Tonisha Crossley";
        nameArray[8] = "Emir Dickerson";
        nameArray[9] = "Zaki Hoffman";

        for(int i = 0; i < 10; i++)
        {
            names.Enqueue(nameArray[i], Random.Range(0, 10));
        }

        while(true)
        {
            string temp = names.Dequeue();

            if (temp == null)
            {
                break;
            }

            Debug.Log(temp);
        }

    }

}
