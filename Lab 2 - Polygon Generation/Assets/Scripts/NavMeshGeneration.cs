﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClipperLib;

namespace Clipper
{
    //setting up path classes
    using Path = List<IntPoint>;
    using Paths = List<List<IntPoint>>;


    //This system is designed to make it easy to generate NavMesh geometry using a series of Vector3 coordinates.
    //In the inspector, one can input vertex values into an array of Vector3s. All they need to do is find where a point they want is
    //I recommend using an emoty game object to do this) and enter it.


    // Notes:
    //Midpoint formula in code:  x = ( (Ax + Bx) / 2, y = (Ay + By) / 2, z = (Az + Bz) / 2 ) 
    public class NavMeshGeneration : MonoBehaviour
    {

        class Edge
        {
            public Vector3 start;
            public Vector3 end;

            Vector3 direction;
            Vector3 normal;

            public Edge(Vector3 aStart, Vector3 aEnd)
            {
                start = aStart;
                end = aEnd;
            }
        }

        // list to store all the Vector3 vertices for the NavMesh
        [SerializeField] List<Vector3> vertices = new List<Vector3>();

        //list storing all of the edges
        List<Edge> edges = new List<Edge>();


        float edgeDistance = 39f;

        Paths paths = new Paths();

        // Start is called before the first frame update
        void Start()
        {
            //create instances of the edge class provided the two points are within a certain distance of each other
            //whether or not the midpoint is over a gap is also a  parameter
            foreach (Vector3 vert in vertices)
            {
                foreach (Vector3 vertex in vertices)
                {
                    if (vert != vertex)
                    {
                        //midpoint x, y, and z
                        float mid_x = (vert.x + vertex.x) / 2;
                        float mid_y = (vert.y + vertex.y) / 2;
                        float mid_z = (vert.z + vertex.z) / 2;

                        //midpoint of the proposed edge
                        Vector3 midpoint = new Vector3(mid_x, mid_y, mid_z);

                        //Ray's origin
                        Vector3 rayOrigin = new Vector3(mid_x, mid_y + 5, mid_z);

                        // Gizmos.DrawSphere(midpoint, .5f);

                        //Ray drawn through the midpoint
                        Ray midRay = new Ray(rayOrigin, Vector3.down);

                        RaycastHit hit = new RaycastHit();

                        // check if a ray drawn from ten units up hits a floor plane
                        if (Physics.Raycast(midRay, out hit))
                        {

                            //distance check
                            if (vertex != vert && Vector3.Distance(vert, vertex) < edgeDistance)
                            {

                                AddEdge(vert, vertex);
                            }

                        }
                    }

                }
            }

            CreatePath(vertices);

            ClipperLib.Clipper.SimplifyPolygon(paths[0], PolyFillType.pftNonZero);



            Debug.Log(ClipperLib.Clipper.Area(paths[0]));
        }

        // Update is called once per frame
        void Update()
        {

        }

        void CleanPolygons(Path aPath, double aDistance)
        {
            aPath = ClipperLib.Clipper.CleanPolygon(aPath, aDistance);
        }

        void CreatePath(List<Vector3> aVertexList)
        {
            //create a Path object
            Path path = new Path();

            //create a list of Vector2s
            List<Vector2> points = new List<Vector2>();

            //create doubles to convert the vertex information into clipper IntPoints
            double x, y = new double();

            //temp Intpoint to use to create a path

            foreach (Vector3 vertex in aVertexList)
            {
                //convert the x and z into doubles. we will be using their 2d data
                x = vertex.x;
                y = vertex.z;

                //create a new IntPoint and add it to the path
                IntPoint intPoint = new IntPoint(x, y);
                path.Add(intPoint);
            }


            // add the path to paths
            paths.Add(path);
        }

        void DrawPolygons()
        {

        }

        //function that adds edges to the list
        void AddEdge(Vector3 start, Vector3 end)
        {
            //create a new edge with the given points
            Edge newEdge = new Edge(start, end);

            //make sure the edge isn't already in the list
            foreach (Edge edge in edges)
            {
                if ((edge.start == start && edge.end == end) || (edge.start == end && edge.end == start))
                {
                    return;
                }

                //is the edge intersecting with one that has already been added
                //if (IsIntersecting(edge, newEdge) == true)
                //{
                //    return;
                //}
            }

            // add the edge to the list
            edges.Add(newEdge);
        }

        //Function to check edge intersection
        bool IsIntersecting(Edge edgeA, Edge edgeB)
        {
            if (edgeA != edgeB)
            {
                //check that the edges do not intersect
                //if (edgeA.start != edgeB.start && edgeA.start != edgeB.end && edgeA.end != edgeB.start && edgeA.end != edgeB.end)
                {
                    {

                        //convert the edge information into 2D so it is easier to calculate
                        Vector2 startA = new Vector2(edgeA.start.x, edgeA.start.z);
                        Vector2 endA = new Vector2(edgeA.end.x, edgeA.end.z);

                        Vector2 startB = new Vector2(edgeB.start.x, edgeB.start.z);
                        Vector2 endB = new Vector2(edgeB.end.x, edgeB.end.z);

                        //calculate the direction and normal vectors
                        Vector2 dirA = (endA - startA).normalized;
                        Vector2 normalA = new Vector2(-dirA.y, dirA.x);

                        Vector2 dirB = (endB - startB).normalized;
                        Vector2 normalB = new Vector2(-dirB.y, dirB.x);

                        //(normal.x * start.x) + (normal.y * start.y)
                        //find k, l for the line equations
                        float k = (normalA.x * startA.x) + (normalA.y * startA.y);
                        float l = (normalB.x * startB.x) + (normalB.y * startB.y);


                        //Calculate the intersection point
                        float xInter = (normalB.y * k - normalA.y * l) / (normalB.x * normalB.y - normalA.y * normalB.x);
                        float yInter = (-normalB.y * k + normalA.x * l) / (normalA.x * normalB.y - normalA.y * normalB.x);

                        Vector2 intersect = new Vector2(xInter, yInter);

                        //check that the intersect is not the start or end points
                        //if (intersect == startA || intersect == startB || intersect == endA || intersect == endB)
                        //{
                        //    return false;
                        //}

                        if (PointIsOnEdge(startA, endA, intersect) && PointIsOnEdge(startB, endB, intersect))
                        {
                            return true;
                        }

                    }

                }

            }
            return false;
        }

        bool PointIsOnEdge(Vector2 start, Vector2 end, Vector2 intersect)
        {
            //length of the edge
            Vector2 length = end - start;

            //distance between vector and intersect
            Vector2 diff = intersect - start;

            //check that the dot product is over zero and the length magnitude is greater than the diff magnitude
            if (Vector2.Dot(length, diff) > 0.0f && length.sqrMagnitude >= diff.sqrMagnitude)
            {
                return true;
            }

            return false;
        }

        Vector3 FindAdjacent(Vector3 vertex)
        {
            Vector3 adj = new Vector3();

            return adj;
        }

        private void OnDrawGizmos()
        {

            //Gizmos.DrawSphere(new Vector3(-1.8f, 0.0f, 3.9f), .5f);
            //Gizmos.DrawSphere(new Vector3(-3.9f, 0.0f, 2.7f), .5f);
            //Gizmos.DrawSphere(new Vector3(-0.3f, 0.0f, 5.1f), .5f);

            Gizmos.color = Color.blue;
            foreach (Path path in paths)
            {
                //draws a sphere on each vertex
                for (int i = 0; i < path.Count; i++)
                {
                    if (i + 1 == path.Count)
                    {
                        Vector3 vertex = new Vector3(path[i].X, 0.0f, path[i].Y);
                        Vector3 to = new Vector3(path[0].X, 0.0f, path[0].Y);
                        Gizmos.DrawSphere(vertex, 0.4f);

                        Gizmos.DrawLine(vertex, to);
                    }
                    else
                    {

                        Vector3 vertex = new Vector3(path[i].X, 0.0f, path[i].Y);
                        Vector3 to = new Vector3(path[i + 1].X, 0.0f, path[i + 1].Y);
                    Gizmos.DrawSphere(vertex, 0.4f);

                        Gizmos.DrawLine(vertex, to);
                    }
                    
                }
            }

            //foreach (Edge edge in edges)
            //{
            //    Gizmos.DrawLine(edge.start, edge.end);
            //}
        }
    }
}