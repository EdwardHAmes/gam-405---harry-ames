﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriorityQueue<DataType>
{
    //class for the elements in the 
    class PriorityQueueElement
    {
        public DataType data;
        public int priority;
    }

    LinkedList<PriorityQueueElement> priorityQueue = new LinkedList<PriorityQueueElement>();
    int size = 0;

    // Add a new value into the priority queue, with the given priority
    public void Enqueue(DataType aData, int aPriority)
    {
        //create an enumerator to iterate through the linked list
        LinkedList<PriorityQueueElement>.Enumerator ienum = priorityQueue.GetEnumerator();

        // create a temporary element to add to the data structure
        PriorityQueueElement newElement = new PriorityQueueElement();

        newElement.priority = aPriority;
        newElement.data = aData;

        while (ienum.MoveNext())
        {
            if(aPriority < ienum.Current.priority)
            {
                priorityQueue.AddBefore(priorityQueue.Find(ienum.Current), newElement);
                size++;
                return;
            }
        }

        priorityQueue.AddLast(newElement);
        size++;
    }

    // Return the element with the lowest priority
    public DataType Dequeue()
    {
        PriorityQueueElement temp = priorityQueue.First.Value;

        priorityQueue.RemoveFirst();

        size--;
        return temp.data;
    }

    public int Size()
    {
        return size;
    }
}
