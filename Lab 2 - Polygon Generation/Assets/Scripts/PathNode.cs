﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour
{
    //public GameObject gameManager;

    public int weight;
    private int index;
    public bool isBlocked = false;

    private void Awake()
    {
        weight = Random.Range(0, 10);


    }

    void Start()
    {
        GameObject.Find("Cube").GetComponent<Graph>().AddNode(transform.position, this);

        float r = ((float)weight * 20f + 30f) / 255f;

        Color color =  new Color(r, 70f / 255f ,25f / 255f);
        GetComponentInParent<Renderer>().material.color = color;

        index = GameObject.Find("Cube").GetComponent<Graph>().GetSize() - 1;
    }
    
  
    public IEnumerator BlockedCoroutine()
    {
        Color color = GetComponentInParent<Renderer>().material.color;
        weight += 100;
        isBlocked = true;
        GetComponentInParent<Renderer>().material.color = Color.red;

        yield return new WaitForSeconds(5.0f);

        weight -= 100;
        isBlocked = false;
        GetComponentInParent<Renderer>().material.color = color;
    }

    public int GetIndex()
    {
        return index;
    }
}
