﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClipperLib;

/// <summary>
/// Much of this NavMesh generation is based on both the clipper library and concepts laid out in Habrador's "Use Math to Solve Problems in Unity"
/// </summary>

namespace Clipper
{
    //setting up path classes
    using Path = List<IntPoint>;
    using Paths = List<List<IntPoint>>;


    //This system is designed to make it easy to generate NavMesh geometry using a series of Vector3 coordinates.
    //In the inspector, one can input vertex values into an array of Vector3s. All they need to do is find where a point they want is
    //I recommend using an empty game object to do this) and enter it.



    // Notes:
    //Midpoint formula in code:  x = ( (Ax + Bx) / 2, y = (Ay + By) / 2, z = (Az + Bz) / 2 ) 
    public class NavMeshGeneration : MonoBehaviour
    {
        //class representing an edge
        class Edge
        {
            public Vertex start;
            public Vertex end;

            Vector3 direction;
            Vector3 normal;

            public Edge(Vector3 aStart, Vector3 aEnd)
            {
                start = new Vertex(aStart);
                end = new Vertex(aEnd);
            }

            public Edge(Vertex aStart, Vertex aEnd)
            {
                start = aStart;
                end = aEnd;
            }
        }

        class Triangle
        {
            //three positions for the triangle points
            Vertex vertex1;
            Vertex vertex2;
            Vertex vertex3;

            //a half edge we can use to generate the triangle
            public HalfEdge halfEdge;

            public List<Vector3> GetVertCoord()
            {
                List<Vector3> coords = new List<Vector3>();
                coords.Add(vertex1.position);
                coords.Add(vertex2.position);
                coords.Add(vertex3.position);

                return coords;
            }

            public Triangle(Vertex A, Vertex B, Vertex C)
            {
                vertex1 = A;
                vertex2 = B;
                vertex3 = C;
            }

            public Triangle(Vector3 A, Vector3 B, Vector3 C)
            {
                vertex1 = new Vertex(A);
                vertex2 = new Vertex(B);
                vertex3 = new Vertex(C);
            }

            public Triangle(HalfEdge aHalfEdge)
            {
                vertex1 = aHalfEdge.start;
                vertex2 = aHalfEdge.end;
                vertex3 = aHalfEdge.pointsToward;
            }
        }

        //class representing a half edge
        class HalfEdge
        {
            //vertex that the edge is facing (opposite vertex of the half edge)
            public Vertex pointsToward;

            //opposite half edge
            public HalfEdge opposite;

            //starting position
            public Vertex start;
            //end position
            public Vertex end;

            public HalfEdge(Vertex aStart, Vertex aEnd, Vertex aPointsToward)
            {
                start = aStart;
                end = aEnd;
                pointsToward = aPointsToward;
            }

            public HalfEdge(Vertex aStart, Vertex aEnd, Vector3 aPointsToward)
            {
                start = aStart;
                end = aEnd;
                pointsToward = new Vertex(aPointsToward);
            }

            public HalfEdge(Vertex aStart, Vertex aEnd, HalfEdge aOpposite)
            {
                start = aStart;
                end = aEnd;
                opposite = aOpposite;
            }

            public HalfEdge(Vector3 aStart, Vector3 aEnd, Vertex aPointsToward)
            {
                start = new Vertex(aStart);
                end = new Vertex(aStart);
                pointsToward = aPointsToward;
            }

            public HalfEdge(Vector3 aStart, Vector3 aEnd, Vector3 aPointsToward)
            {
                start = new Vertex(aStart);
                end = new Vertex(aStart);
                pointsToward = new Vertex(aPointsToward);
            }

            public HalfEdge(Vector3 aStart, Vector3 aEnd, HalfEdge aOpposite)
            {
                start = new Vertex(aStart);
                end = new Vertex(aStart);
                opposite = aOpposite;
            }

        }

        //class for a vertex
        class Vertex
        {
            //enum for whether it convex, concave, or an 'ear'
            public enum Attribute
            {
                CONVEX = 1,
                CONCAVE = 2,
                EAR = 4
            }

            public Attribute attribute;

            //position of the vertex
            public Vector3 position;

            //the previous and next vertices
            public Vertex next;
            public Vertex previous;

            //returns the 2D position of the vertex
            public Vector2 Get2DPosition()
            {
                return new Vector2(position.x, position.z);
            }

            public Vertex(Vector3 aPosition)
            {
                position = aPosition;
            }
        }

        // list to store all the Vector3 points for the NavMesh
        [SerializeField] List<Vector3> points = new List<Vector3>();

        //list storing all of the edges
        List<Edge> edges = new List<Edge>();


        float edgeDistance = 39f;

        Paths paths = new Paths();

        List<Triangle> triangles = new List<Triangle>();
        List<HalfEdge> halfEdges = new List<HalfEdge>();
        List<Vertex> verts = new List<Vertex>();

        // Start is called before the first frame update
        void Start()
        {
            //create instances of the edge class provided the two points are within a certain distance of each other
            //whether or not the midpoint is over a gap is also a  parameter
            foreach (Vector3 p in points)
            {
                foreach (Vector3 point in points)
                { 
                    if (p != point)
                    {
                        //midpoint x, y, and z
                        float mid_x = (p.x + point.x) / 2;
                        float mid_y = (p.y + point.y) / 2;
                        float mid_z = (p.z + point.z) / 2;

                        //midpoint of the proposed edge
                        Vector3 midpoint = new Vector3(mid_x, mid_y, mid_z);

                        //Ray's origin
                        Vector3 rayOrigin = new Vector3(mid_x, mid_y + 5, mid_z);

                        // Gizmos.DrawSphere(midpoint, .5f);

                        //Ray drawn through the midpoint
                        Ray midRay = new Ray(rayOrigin, Vector3.down);

                        RaycastHit hit = new RaycastHit();

                        // check if a ray drawn from ten units up hits a floor plane
                        if (Physics.Raycast(midRay, out hit))
                        {

                            //distance check
                            if (point != p && Vector3.Distance(p, point) < edgeDistance)
                            {
                                //add the edge to the data structure
                                AddEdge(p, point);
                            }

                        }
                    }

                }
            }

            //create a clipper path
            CreatePath(points);

            //simplify the polygon to make the base area
            ClipperLib.Clipper.SimplifyPolygon(paths[0], PolyFillType.pftNonZero);

            //update the edges based on the simplified polygon
            UpdateEdgesFromPath(paths[0]);

            triangles = Triangulate(verts);

            Debug.Log("Vert Count: " + verts.Count);
            Debug.Log("Edge Count: " + edges.Count);
            Debug.Log("Tri Count: " + triangles.Count);
            Debug.Log("Path Count: " + paths[0].Count);

        }

        // Update is called once per frame
        void Update()
        {

        }

        void UpdateEdgesFromPath(Path aPath)
        {
            List<Edge> tempEdges = new List<Edge>();
            List<Vertex> tempVerts = new List<Vertex>();

            //create instances of our vertex class for each intpoint in our path
            for (int i = 0; i < aPath.Count; i++)
            {
                float x = paths[0][i].X;
                float z = paths[0][i].Y;

                x = x / 1000000;
                z = z / 1000000;

                Vertex vert = new Vertex(new Vector3(x, 0.0f, z));

                tempVerts.Add(vert);
            }

            //reset the vert list based on the path
            verts = tempVerts;

            //make edges based 
            for (int i = 0; i < verts.Count; i++)
            {
                // if we are at the end of the list, make the start of the edge the last vert, and the end the first vert in the list
                if (i + 1 == verts.Count)
                {
                    Vertex start = verts[i];
                    Vertex end = verts[0];

                    Edge temp = new Edge(start, end);
                    tempEdges.Add(temp);
                }

                else
                {
                    Vertex start = verts[i];
                    Vertex end = verts[i + 1];

                    Edge temp = new Edge(start, end);
                    tempEdges.Add(temp);
                }
            }

            //reset the edges based on the path
            edges = tempEdges;
        }

        void CleanPolygons(Path aPath, double aDistance)
        {
            aPath = ClipperLib.Clipper.CleanPolygon(aPath, aDistance);
        }

        void CreatePath(List<Vector3> aVertexList)
        {
            //create a Path object
            Path path = new Path();

            //create a list of Vector2s
            List<Vector2> points = new List<Vector2>();

            //create doubles to convert the vertex information into clipper IntPoints
            double x, y = new double();

            //temp Intpoint to use to create a path

            foreach (Vector3 vertex in aVertexList)
            {
                //convert the x and z into doubles. we will be using their 2d data
                x = vertex.x * 1000000;
                y = vertex.z * 1000000;

                //create a new IntPoint and add it to the path
                IntPoint intPoint = new IntPoint(x, y);
                path.Add(intPoint);
            }


            // add the path to paths
            paths.Add(path);
        }


        //function that adds edges to the list
        void AddEdge(Vector3 start, Vector3 end)
        {
            //create a new edge with the given points
            Edge newEdge = new Edge(start, end);

            //make sure the edge isn't already in the list
            foreach (Edge edge in edges)
            {
                if ((edge.start.position == start && edge.end.position == end) || (edge.start.position == end && edge.end.position == start))
                {
                    return;
                }

                //is the edge intersecting with one that has already been added
                //if (IsIntersecting(edge, newEdge) == true)
                //{
                //    return;
                //}
            }

            // add the edge to the list
            edges.Add(newEdge);
        }

        //Function to check edge intersection
        bool IsIntersecting(Edge edgeA, Edge edgeB)
        {
            if (edgeA != edgeB)
            {
                //check that the edges do not intersect
                //if (edgeA.start != edgeB.start && edgeA.start != edgeB.end && edgeA.end != edgeB.start && edgeA.end != edgeB.end)
                {
                    {

                        //convert the edge information into 2D so it is easier to calculate
                        Vector2 startA = new Vector2(edgeA.start.position.x, edgeA.start.position.z);
                        Vector2 endA = new Vector2(edgeA.end.position.x, edgeA.end.position.z);

                        Vector2 startB = new Vector2(edgeB.start.position.x, edgeB.start.position.z);
                        Vector2 endB = new Vector2(edgeB.end.position.x, edgeB.end.position.z);

                        //calculate the direction and normal vectors
                        Vector2 dirA = (endA - startA).normalized;
                        Vector2 normalA = new Vector2(-dirA.y, dirA.x);

                        Vector2 dirB = (endB - startB).normalized;
                        Vector2 normalB = new Vector2(-dirB.y, dirB.x);

                        //(normal.x * start.x) + (normal.y * start.y)
                        //find k, l for the line equations
                        float k = (normalA.x * startA.x) + (normalA.y * startA.y);
                        float l = (normalB.x * startB.x) + (normalB.y * startB.y);


                        //Calculate the intersection point
                        float xInter = (normalB.y * k - normalA.y * l) / (normalB.x * normalB.y - normalA.y * normalB.x);
                        float yInter = (-normalB.y * k + normalA.x * l) / (normalA.x * normalB.y - normalA.y * normalB.x);

                        Vector2 intersect = new Vector2(xInter, yInter);

                        //check that the intersect is not the start or end points
                        //if (intersect == startA || intersect == startB || intersect == endA || intersect == endB)
                        //{
                        //    return false;
                        //}

                        if (PointIsOnEdge(startA, endA, intersect) && PointIsOnEdge(startB, endB, intersect))
                        {
                            return true;
                        }

                    }

                }

            }
            return false;
        }

        bool PointIsOnEdge(Vector2 start, Vector2 end, Vector2 intersect)
        {
            //length of the edge
            Vector2 length = end - start;

            //distance between vector and intersect
            Vector2 diff = intersect - start;

            //check that the dot product is over zero and the length magnitude is greater than the diff magnitude
            if (Vector2.Dot(length, diff) > 0.0f && length.sqrMagnitude >= diff.sqrMagnitude)
            {
                return true;
            }

            return false;
        }

        Vector3 FindAdjacent(Vector3 vertex)
        {
            Vector3 adj = new Vector3();

            return adj;
        }

        //this function triangulates a given list of ordered vertices as a polygon
        List<Triangle> Triangulate(List<Vertex> someVerts)
        {
            //list of tris that will be returned
            List<Triangle> tris = new List<Triangle>();

            //if there are only 3 verts, make a triangle and return it
            if (someVerts.Count == 3)
            {
                //create a tri from the verts
                Triangle temp = new Triangle(someVerts[0], someVerts[1], someVerts[2]);

                //add it to the list
                tris.Add(temp);

                //return the list
                return tris;
            }

            //ToDo:
            //find the adjacent nodes
            for (int i = 0; i < someVerts.Count; i++)
            {
                someVerts[i].previous = someVerts[(((i - 1) % someVerts.Count) + someVerts.Count) % someVerts.Count];
                someVerts[i].next = someVerts[(((i + 1) % someVerts.Count) + someVerts.Count) % someVerts.Count];
            }

            //check if the vert is convex or concave
            foreach (Vertex vert in someVerts)
            {
                //if the tri made with this vert and the previous and next is oriented clockwise or counter-clockwise
                if (TriIsClockwise(vert.previous, vert, vert.next))
                {
                    vert.attribute = Vertex.Attribute.CONCAVE;
                }
                else
                {
                    vert.attribute = Vertex.Attribute.CONVEX;
                }
            }

            //find the ears
            //start with a new list of verts
            List<Vertex> earVerts = new List<Vertex>();

            //check if the vert is an ear. this may be able to be condensed into the previous loop
            foreach (Vertex vert in someVerts)
            {
                //Concave verts cannot be ears, so ensure that the ear logic only happens if the vert is not reflexive
                if (vert.attribute != Vertex.Attribute.CONCAVE || vert.attribute != (Vertex.Attribute.CONCAVE & Vertex.Attribute.EAR))
                {
                    bool hasPointInside = false;

                    // we want to check that a reflexive point is not in the triangle created by the vert and its prev and next
                    foreach (Vertex v in someVerts)
                    {
                        if (vert.attribute == Vertex.Attribute.CONCAVE)
                        {
                            //check if the vert is inside the triangle
                            if (PointIsInTri(vert.previous, vert, vert.next, v))
                            {
                                hasPointInside = true;
                                break;
                            }
                        }
                    }

                    //if the tri does not have a point, it is an ear
                    if (hasPointInside == false)
                    {
                        vert.attribute = vert.attribute & Vertex.Attribute.EAR;

                        //add it to the list of ear verts
                        earVerts.Add(vert);
                    }
                }

                //if for some reason the attribute was changed so that it is both concave and an ear, make it just concave
                if (vert.attribute != (Vertex.Attribute.CONCAVE & Vertex.Attribute.EAR))
                {
                    vert.attribute = Vertex.Attribute.CONCAVE;
                }
            }

            //copy the list of verts so we can remove them safely
            List<Vertex> tempVerts = new List<Vertex>();
            foreach(Vertex vert in verts)
            {
                tempVerts.Add(new Vertex(vert.position));
            }
            if (earVerts.Count != 0)
            {
                //loop through the list until you are left with 3 verts
                while (tempVerts.Count > 3)
                {

                    //store the first element in the ear list
                    Vertex ear = earVerts[0];

                    //we need to store its previous and next verts as well, so we can update them on removal of the ear
                    Vertex earNext = ear.next;
                    Vertex earPrev = ear.previous;

                    //create a tri and add it to the list of tris
                    Triangle temp = new Triangle(ear, earPrev, earNext);

                    //update the vertex information 
                    earNext.previous = earPrev;
                    earPrev.next = earNext;

                    //remove the ear from the lists
                    tempVerts.Remove(ear);
                    earVerts.Remove(ear);

                    //update whether the ear's previous and next are concave or convex
                    if (TriIsClockwise(earPrev.previous, earPrev, earPrev.next))
                    {
                        earPrev.attribute = Vertex.Attribute.CONCAVE;
                    }
                    else
                    {
                        earPrev.attribute = Vertex.Attribute.CONVEX;
                    }

                    if (TriIsClockwise(earNext.previous, earNext, earNext.next))
                    {
                        earNext.attribute = Vertex.Attribute.CONCAVE;
                    }
                    else
                    {
                        earNext.attribute = Vertex.Attribute.CONVEX;
                    }

                    //check if they are ears
                    earVerts.Remove(earNext);
                    earVerts.Remove(earPrev);

                    if (IsEar(earNext, someVerts))
                    {
                        earVerts.Add(earNext);
                    }

                    if (IsEar(earPrev, someVerts))
                    {
                        earVerts.Add(earPrev);
                    }

                }
            }

            //add the last tri
            tris.Add(new Triangle(verts[0], verts[0].previous, verts[0].next));

            return tris;
        }

        bool IsEar(Vertex aVert, List<Vertex> someVerts)
        {
            //Concave verts cannot be ears, so ensure that the ear logic only happens if the vert is not reflexive
            if (aVert.attribute != Vertex.Attribute.CONCAVE || aVert.attribute != (Vertex.Attribute.CONCAVE & Vertex.Attribute.EAR))
            {
                // bool hasPointInside = false;

                // we want to check that a reflexive point is not in the triangle created by the vert and its prev and next
                foreach (Vertex v in someVerts)
                {
                    if (aVert.attribute == Vertex.Attribute.CONCAVE)
                    {
                        //check if the vert is inside the triangle
                        if (PointIsInTri(aVert.previous, aVert, aVert.next, v))
                        {
                            //hasPointInside = true;
                            return false;
                        }
                    }
                }

                //if the tri does not have a point, it is an ear
                aVert.attribute = aVert.attribute & Vertex.Attribute.EAR;

                return true;
            }

            return false;
        }


        bool PointIsInTri(Vertex A, Vertex B, Vertex C, Vertex aPoint)
        {
            //get the vector2s for the vertex 2D positions
            Vector2 a = A.Get2DPosition();
            Vector2 b = B.Get2DPosition();
            Vector3 c = C.Get2DPosition();
            Vector3 p = aPoint.Get2DPosition();

            //barycentric formulas obtained from habrador
            float d = ((b.y - c.y) * (a.x - c.x) + (c.x - b.x) * (a.y - c.y));

            float aBary = ((b.y - c.y) * (p.x - c.x) + (c.x - b.x) * (p.y - c.y)) / d;
            float bBary = ((c.y - a.y) * (p.x - c.x) + (a.x - c.x) * (p.y - c.y)) / d;
            float cBary = 1 - aBary - bBary;

            //check if the point is within the bounds. If so, return true
            if (aBary > 0f && aBary < 1f && bBary > 0f && bBary < 1f && cBary > 0f && cBary < 1f)
            {
                return true;
            }

            return false;
        }

        bool TriIsClockwise(Vertex A, Vertex B, Vertex C)
        {
            //Get the 2D positions of the vertices
            Vector2 a2D = A.Get2DPosition();
            Vector2 b2D = B.Get2DPosition();
            Vector2 c2D = C.Get2DPosition();

            //find the determinite of the tri
            float det = a2D.x * b2D.y + c2D.x * a2D.y + b2D.x * c2D.y - a2D.x * c2D.y - c2D.x * b2D.y - b2D.x * a2D.y;

            //if the determinite is greater than zero, it means that the tri is oriented counterclockwise
            if (det > 0)
                return false;


            return true;
        }

        private void OnDrawGizmos()
        {

            //Gizmos.DrawSphere(new Vector3(-1.8f, 0.0f, 3.9f), .5f);
            //Gizmos.DrawSphere(new Vector3(-3.9f, 0.0f, 2.7f), .5f);
            //Gizmos.DrawSphere(new Vector3(-0.3f, 0.0f, 5.1f), .5f);

            Gizmos.color = Color.blue;

            //draws a sphere on each vertex, nd lines connecting you
            for (int i = 0; i < verts.Count; i++)
            {
                Gizmos.DrawSphere(verts[i].position, 0.4f);
            }

            //foreach (Edge edge in edges)
            //{
            //    Gizmos.DrawLine(edge.start.position, edge.end.position);
            //}
            //foreach (Edge edge in edges)
            //{
            //    Gizmos.DrawLine(edge.start, edge.end);
            //}

            foreach(Triangle tri in triangles)
            {
                List<Vector3> coords = tri.GetVertCoord();
                Gizmos.DrawLine(coords[0], coords[1]);
                Gizmos.DrawLine(coords[1], coords[2]);
                Gizmos.DrawLine(coords[2], coords[0]);
            }
        }
    }
}