﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Graph : MonoBehaviour
{
    //node that will be contained in the graph. Contains a PathNode and a list of adjacent nodes
    public class Node
    {
        public PathNode pathNode;
        public List<Node> adjacentNodes = new List<Node>();
        //public float value;
    }

    List<Node> graph = new List<Node>();
    int size = 0;

    //float to check distance with
    [SerializeField] float adjacentNodeDistance;

    public List<PathNode> path = new List<PathNode>();
    public int pathIndex = 0;

    public bool isMoving = true;

    //speed of the player while it is lerping through the path
    public float speed;

    PathNode currentPos;

    private void Update()
    {
        // if the player has a path to follow
        if (isMoving && path.Count != 0)
        {
            //vector to hold the player's target position


            // set the target to the specific node in the path
            Vector3 target = new Vector3(path[pathIndex].transform.position.x, transform.position.y, path[pathIndex].transform.position.z);

            //lerp to the target
            transform.position = Vector3.Lerp(transform.position, target, speed * Time.deltaTime);

            // if the player has reached its destination, then incriment the pathIndex
            if (transform.position == target)
            {

                pathIndex++;
            }
        }

    }


    //function to add a node to the dictionary
    public void AddNode(Vector3 argKey, PathNode argPathNode)
    {
        //ToDo:

        //Debug.Log("Add Function is runnning");

        //create a dummy node that will be added to the dictionary
        Node tempNode = new Node();

        //set the Node.pathNode variable to equal the passed in node
        tempNode.pathNode = argPathNode;

        //inciment the size
        size++;

        //loop through the dictionary, checking that the positions of the pathNodes are within a certain distance with each other. Add those nodes to the list of connected nodes
        foreach (Node node in graph)
        {
            //check if the key is equal to a key already in the dictionary, and that the new node is within the connection distance
            if (node.pathNode != tempNode.pathNode &&
               Vector3.SqrMagnitude(node.pathNode.transform.position - tempNode.pathNode.transform.position) <= adjacentNodeDistance * adjacentNodeDistance)
            {
                //if the nodes are not already connected, connect them 
                if (!node.adjacentNodes.Contains(tempNode) && !tempNode.adjacentNodes.Contains(node))
                {
                    tempNode.adjacentNodes.Add(node);
                    node.adjacentNodes.Add(tempNode);
                }
            }
        }

        //add the node to the dictionary
        graph.Add(tempNode);

        //Debug.Log(graph.Count);
    }

    public int GetSize()
    {
        return size;
    }

    public List<Node> GetGraph()
    {
        return graph;
    }
    int Heuristic(PathNode aNode, PathNode end)
    {
        return (int)Vector3.Distance(aNode.transform.position, end.transform.position);
    }

    public List<PathNode> FindPath(PathNode start, PathNode end)
    {
        Debug.Log("Start: node " + start.GetIndex());
        Debug.Log("End: node " + end.GetIndex());

        //list to store the actual path
        //List<PathNode> path = new List<PathNode>();

        if (start != end)
        {
            //our frontier, which we will use a Proiority Queue for
            PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>();

            //list of visited nodes
            //List<PathNode> visited = new List<PathNode>();

            //Dictionary representing the node that the stored node camefrom
            Dictionary<PathNode, PathNode> cameFrom = new Dictionary<PathNode, PathNode>();

            //dictionary to store the cost to get to the node
            Dictionary<PathNode, int> costSoFar = new Dictionary<PathNode, int>();

            //add the start to the frontier
            frontier.Enqueue(start, 0);
            costSoFar.Add(start, 0);

            //visited.Add(start);



            //main algorithm loop
            while (frontier.Size() != 0)
            {
                //dequeue the frontier as our next node
                PathNode next = frontier.Dequeue();

                //loop early out
                if (next == end)
                {
                    break;
                }

                //loop through next's connections
                foreach (Node connection in graph[next.GetIndex()].adjacentNodes)
                {
                    //if the connections haven't been visited, or if the cost so far is less than that stored, visit the node and update the cost.
                    if (!costSoFar.ContainsKey(connection.pathNode) || costSoFar[connection.pathNode] > costSoFar[next] + connection.pathNode.weight)
                    {
                        frontier.Enqueue(connection.pathNode, connection.pathNode.weight + Heuristic(connection.pathNode, end));
                        //visited.Add(connection.pathNode);
                        cameFrom[connection.pathNode] =  next;
                        costSoFar[connection.pathNode] = costSoFar[next] + connection.pathNode.weight;

                    }
                }

            }

            //list storing the inverted path
            List<PathNode> invertPath = new List<PathNode>();

            //key for accessing the camefrom dictionary, initialized at end
            PathNode key = end;

            //add end
            invertPath.Add(end);

            //loop as long as the dictionary contains the key
            while (key != start)
            {

                key = cameFrom[key];
                invertPath.Add(key);
            }



            //straighten out the path
            for (int i = 1; i < invertPath.Count; i++)
            {
                path.Add(invertPath[invertPath.Count - i]);
            }

            path.Add(end);
            return path;

        }

        else
        {
            path.Add(start);
            return path;
        }
    }

    public PathNode FindClosestNode(Transform parent)
    {
        return GetComponentInChildren<PathNode>();
    }

    //private void OnMouseDown()


    private void OnDrawGizmos()
    {
        //check that there are actually nodes in the graph
        if (graph.Count > 0)
        {


            //for every node in the graph, draw a sphere
            foreach (Node node in graph)
            {
                Gizmos.DrawSphere(node.pathNode.transform.position, 0.25f);

                //for every connection in every node, draw a line
                foreach (Node adjNode in node.adjacentNodes)
                {
                    Gizmos.DrawLine(node.pathNode.transform.position, adjNode.pathNode.transform.position);
                }
            }

            if (path.Count > 0)
            {
                Gizmos.color = Color.red;

                for (int i = 0; i < path.Count; i++)
                {
                    Gizmos.DrawSphere(path[i].transform.position, 0.3f);
                    if (i + 1 < path.Count)
                    {
                        Gizmos.DrawLine(path[i].transform.position, path[i + 1].transform.position);
                    }
                }
            }
        }
    }
}
