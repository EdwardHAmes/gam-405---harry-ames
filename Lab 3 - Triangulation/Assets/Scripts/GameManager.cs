﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //variables to determine the length/width of the map
    [SerializeField] int gridLength;
    [SerializeField] int gridWidth;

    //stores the tile prefab
    public GameObject mapTile;
    public GameObject player;

    private void Awake()
    {
        //dummy Vector3 for instantiating tiles
        Vector3 spawnPos = new Vector3();


        //instantiate the map
        for (int y = 0; y < gridLength; y++)
        {
            //set the spawnpos z value as the loop iteration we are on
            spawnPos.z = y;

            for (int x = 0; x < gridWidth; x++)
            {
                //set the spawnpos x value as the loop iteration we are on
                spawnPos.x = x;

                //Instantiate the map tile
                Instantiate(mapTile, spawnPos, Quaternion.identity);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("mouse has been clicked");

            //clear the current path
            player.GetComponent<Graph>().path.Clear();

            // raycast it object for the map tile that is clicked on
            RaycastHit hit;

            //Raycast from the camera to the mouse position
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {

                if (hit.collider.tag == "MapTile")
                {
                    //Debug.Log(hit.transform.position);
                    RaycastHit playerTileHit;

                    if (Physics.Raycast(player.transform.position, Vector3.down, out playerTileHit))
                    {
                        // Debug.Log(hit.transform.name);
                        player.GetComponent<Graph>().FindPath(playerTileHit.transform.GetComponentInChildren<PathNode>(), hit.transform.GetComponentInChildren<PathNode>());
                    }

                    

                    
                }
            }




            player.GetComponent<Graph>().pathIndex = 0;
            player.GetComponent<Graph>().isMoving = true;
            //Debug.Log(path.Count);
        }

        if (Input.GetMouseButtonDown(1))
        {
            //ToDo:
            //Recieve the pathnode that you clicked on
            PathNode target = new PathNode();

            RaycastHit hit;

            //Raycast from the camera to the mouse position
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                if (hit.collider.tag == "MapTile")
                {
                    Debug.Log(hit.transform.position);

                    //find the end
                    target = hit.transform.GetComponentInChildren<PathNode>();
                }
            }

            target.StartCoroutine("BlockedCoroutine");

            //foreach connection in the node, adjust the weigh
            foreach (Graph.Node connection in player.GetComponent<Graph>().GetGraph()[target.GetIndex()].adjacentNodes)
            {
                connection.pathNode.StartCoroutine("BlockedCoroutine");
            }

        }
    }
}
