﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavMesh : MonoBehaviour
{
    //classes for verts, half edges, and tris
    public class Vertex
    {
        //verts have a position, and store the next a previous verts
        public Vector3 pos;
        public Vertex next;
        public Vertex previous;

        //constructors
        public Vertex(float x, float y, float z)
        {
            pos = new Vector3(x, y, z);
        }

        public Vertex(Vector3 aPosition)
        {
            pos = aPosition;
        }

        //function that returns the vertex's 2D position
        public Vector2 Get2DPos()
        {
            return new Vector2(pos.x, pos.z);
        }
    }

    public class HalfEdge
    {
        //half edges will store a start and end, and vertex that it is facing
        public Vertex start;
        public Vertex end;
        public Vertex pointsTo;

        //constructors
        public HalfEdge(Vertex aStart, Vertex aEnd, Vertex isFacing)
        {
            start = aStart;
            end = aEnd;
            pointsTo = isFacing;
        }

        public HalfEdge(Vector3 aStartPos, Vector3 aEndPos, Vector3 isFacingPos)
        {
            start = new Vertex(aStartPos);
            end = new Vertex(aEndPos);
            pointsTo = new Vertex(isFacingPos);
        }
    }

    public class Triangle
    {
        //three points
        public Vertex A;
        public Vertex B;
        public Vertex C;

        //stores a half edge as well
        public HalfEdge halfEdge;

        //three constructors
        public Triangle(Vertex a, Vertex b, Vertex c)
        {
            A = a;
            B = b;
            C = c;
        }

        public Triangle(Vector3 aPos, Vector3 bPos, Vector3 cPos)
        {
            A = new Vertex(aPos);
            B = new Vertex(bPos);
            C = new Vertex(cPos);
        }

        public Triangle( HalfEdge aHalfEdge)
        {
            A = aHalfEdge.pointsTo;
            B = aHalfEdge.start;
            C = aHalfEdge.end;

            halfEdge = aHalfEdge;
        }

        //function that changes how the tri is oriented
        public void ChangeOrientation()
        {
            //create a temp vert and set it equal to A
            Vertex temp = A;

            //set A equal to B
            A = B;

            //set B equal to temp
            B = temp;
        }
    }

    public class Edge
    {
        //two verts for the start and end
        Vertex start;
        Vertex end;

        //constructors
        public Edge(Vertex aStart, Vertex aEnd)
        {
            start = aStart;
            end = aEnd;
        }

        public Edge(Vector3 aStartPos, Vector3 aEndPos)
        {
            start = new Vertex(aStartPos);
            end = new Vertex(aEndPos);
        }
    }

    //we will store a list of GameObjects to get where our NavMesh should be
    [SerializeField] List<GameObject> navMeshPoints = new List<GameObject>();

    //a list of verts
    List<Vertex> allVerts = new List<Vertex>();


    private void Awake()
    {
        foreach(GameObject point in navMeshPoints)
        {
            GetComponent<MeshRenderer>().enabled = false;
            allVerts.Add(new Vertex(point.transform.position));
        }
    }

    // Start is called before the first frame update
    void Start()
    { 

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //triangulates the vertices
    public List<Triangle> Triagulate(List<Vertex> someVerts)
    {
        //create the list of tris that we will return
        List<Triangle> someTris = new List<Triangle>();

        //if the vert count is 3, add a triangle with the three verts and return
        if(someVerts.Count == 3)
        {
            someTris.Add(new Triangle(someVerts[0], someVerts[1], someVerts[2]));
            return someTris;
        }

        //find the next and previous vertex for each vertex in the list
        for(int i = 0; i < someVerts.Count; i++)
        {
            someVerts[i].next = someVerts[ClampIndex(i + 1, someVerts.Count)];
            someVerts[i].previous = someVerts[ClampIndex(i - 1, someVerts.Count)];
        }

        return someTris;
    }
    
    //function clamps an index
    public int ClampIndex(int index, int size)
    {
        return ((index % size) + size) % size;
    }

    private void OnDrawGizmos()
    {
        foreach(Vertex vert in allVerts)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(vert.pos, .3f);
        }
    }
}
