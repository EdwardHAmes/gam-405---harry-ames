﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Graph : MonoBehaviour
{
    //node that will be contained in the graph. Contains a PathNode and a list of adjacent nodes
    class Node
    {
        public PathNode pathNode;
        public List<Node> adjacentNodes = new List<Node>();
    }

    Dictionary<Vector3, Node> graph = new Dictionary<Vector3, Node>();

    //float to check distance with
    [SerializeField] float adjacentNodeDistance;

    //variables to determine the length/width of the map
    [SerializeField] int gridLength;
    [SerializeField] int gridWidth;

    //stores the tile prefab
    public GameObject mapTile;

    private void Awake()
    {
        //dummy Vector3 for instantiating tiles
        Vector3 spawnPos = new Vector3();


        //instantiate the map
        for (int y = 0; y < gridLength; y++)
        {
            //set the spawnpos z value as the loop iteration we are on
            spawnPos.z = y;

            for (int x = 0; x < gridWidth; x++)
            {
                //set the spawnpos x value as the loop iteration we are on
                spawnPos.x = x;

                //Instantiate the map tile
                Instantiate(mapTile, spawnPos, Quaternion.identity);
            }
        }
    }

    //function to add a node to the dictionary
    public void AddNode(Vector3 argKey, PathNode argPathNode)
    {
        //ToDo:

        Debug.Log("Add Function is runnning");

        //create a dummy node that will be added to the dictionary
        Node tempNode = new Node();

        //set the Node.pathNode variable to equal the passed in node
        tempNode.pathNode = argPathNode;

        //loop through the dictionary, checking that the positions of the pathNodes are within a certain distance with each other. Add those nodes to the list of connected nodes
        foreach (KeyValuePair<Vector3, Node> node in graph)
        {
            //check if the key is equal to a key already in the dictionary, and that the new node is within the connection distance
            if (node.Key != argKey && 
               Vector3.SqrMagnitude(node.Key - tempNode.pathNode.transform.position) <= adjacentNodeDistance * adjacentNodeDistance)
            {
                //if the nodes are not already connected, connect them 
                if (!node.Value.adjacentNodes.Contains(tempNode) && !tempNode.adjacentNodes.Contains(node.Value))
                {
                    tempNode.adjacentNodes.Add(node.Value);
                    node.Value.adjacentNodes.Add(tempNode);
                }
            }
        }

        //add the node to the dictionary
        graph.Add(argKey, tempNode);

        Debug.Log(graph.Count);
    }

    private void OnDrawGizmos()
    {
        //check that there are actually nodes in the graph
        if (graph.Count > 0)
        {


            //for every node in the graph, draw a sphere
            foreach (KeyValuePair<Vector3, Node> node in graph)
            {
                Gizmos.DrawSphere(node.Key, 0.25f);

                //for every connection in every node, draw a line
                foreach (Node adjNode in node.Value.adjacentNodes)
                {
                    Gizmos.DrawLine(node.Value.pathNode.transform.position, adjNode.pathNode.transform.position);
                }
            }
        }
    }
}
