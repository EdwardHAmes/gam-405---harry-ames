﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "harryAmesAI", menuName = "AI/Harry Ames's AI")]
public class HarryAmesAI : BaseAI
{

    class MapTile
    {
        public int tileID;

        //adjacent tiles
        public MapTile up;
        public MapTile down;
        public MapTile left;
        public MapTile right;

        public GameManager.SensorData data;

        public MapTile(int aKey, GameManager.SensorData aData)
        {
            tileID = aKey;
            data = aData;

            up = null;
            down = null;
            left = null;
            right = null;
        }
    }

    MapTile current = null;

    //key that we assign upon creating a new tile
    int keys = 0;

    MapTile goal = null;
    MapTile diamond = null;

    List<MapTile> mapData = new List<MapTile>();

    //does the player have the diamond?
    bool hasDiamond = false;

    //our version of the GetAction() function
    public override CombatantAction GetAction(ref List<GameManager.Direction> aMoves, ref int aBombTime)
    {
        //if there is no data on the current map tile, then it must be the first turn, so lets get the initial information
        if (mapData.Count == 0 || current == null)
        {
            current = CreateTile(UseSensor(GameManager.Direction.Current), GameManager.Direction.Current);
            mapData.Add(current);

            current.down = CreateTile(UseSensor(GameManager.Direction.Down), GameManager.Direction.Down);
            mapData.Add(current.left);

            current.up = CreateTile(UseSensor(GameManager.Direction.Up), GameManager.Direction.Up);
            mapData.Add(current.right);

            current.left = CreateTile(UseSensor(GameManager.Direction.Left), GameManager.Direction.Left);
            mapData.Add(current.up);

            current.right = CreateTile(UseSensor(GameManager.Direction.Right), GameManager.Direction.Right);
            mapData.Add(current.down);
        }

        //check each direction to ensure that we have data on all of these tiles, otherwise just update the data
        if (current.up == null)
        {
            current.up = CreateTile(UseSensor(GameManager.Direction.Up), GameManager.Direction.Up);
            Debug.Log("Up is Checked");
        }
        else
        {
            current.up.data = UseSensor(GameManager.Direction.Up);

        }

        if (current.down == null)
        {
            current.down = CreateTile(UseSensor(GameManager.Direction.Down), GameManager.Direction.Down);
            Debug.Log("Down is Checked");
        }
        else
        {
            current.down.data = UseSensor(GameManager.Direction.Down);
        }

        if (current.left == null)
        {
            current.left = CreateTile(UseSensor(GameManager.Direction.Left), GameManager.Direction.Left);
            Debug.Log("Left is Checked");
        }
        else
        {
            current.left.data = UseSensor(GameManager.Direction.Left);
        }

        if (current.right == null)
        {
            current.right = CreateTile(UseSensor(GameManager.Direction.Right), GameManager.Direction.Right);
            Debug.Log("Right is Checked");
        }
        else
        {
            current.right.data = UseSensor(GameManager.Direction.Right);
        }




        //drop a bomb if the enemies are nearby
        if (CheckForEnemies())
        {
            //set the timer for 2
            aBombTime = 2;
            return CombatantAction.DropBomb;
        }



        //find the direction that we should move
        GameManager.Direction direction = GetDirection();

        Debug.Log("DirectionToMove: " + direction);

        //if the direction isn't current return
        if (direction != GameManager.Direction.Current)
        {
            aMoves.Add(direction);
            return CombatantAction.Move;
        }

        else
        {
            Debug.Log("Passed");
            return CombatantAction.Pass;
        }
    }

    //checks if there is an enemy next to you
    bool CheckForEnemies()
    {
        if (current.left.data == GameManager.SensorData.Enemy ||
           current.right.data == GameManager.SensorData.Enemy ||
           current.down.data == GameManager.SensorData.Enemy ||
           current.up.data == GameManager.SensorData.Enemy)
        {
            return true;
        }

        return false;
    }

    //checks for the diamond
    bool CheckForDiamond(MapTile aTile)
    {
        if (aTile.data == GameManager.SensorData.Diamond)
        {
            diamond = aTile;
            hasDiamond = true;
            return true;
        }

        return false;
    }

    //checks for the goal
    bool CheckForGoal(MapTile aTile)
    {
        if (aTile.data == GameManager.SensorData.Diamond)
        {
            diamond = aTile;
            hasDiamond = true;
            return true;
        }

        return false;
    }

    bool TileIsObstructed(MapTile aTile)
    {
        if (aTile.data == GameManager.SensorData.Wall ||
            aTile.data == GameManager.SensorData.OffGrid ||
            aTile.data == GameManager.SensorData.Bomb)
        {
            return true;
        }
        return false;
    }

    //Returns the next direction to move
    GameManager.Direction GetDirection()
    {
        //list of possible tiles to move to. Tiles will be removed if they are obstructed
        List<MapTile> possibleTiles = new List<MapTile>();

        //add the currents directional tiles to the list, but only if not obstructed
        if (!TileIsObstructed(current.up))
        {
            possibleTiles.Add(current.up);
        }

        if (!TileIsObstructed(current.down))
        {
            possibleTiles.Add(current.down);
        }

        if (!TileIsObstructed(current.left))
        {
            possibleTiles.Add(current.left);
        }

        if (!TileIsObstructed(current.right))
        {
            possibleTiles.Add(current.right);
        }

        //check for the diamond in the possible tiles
        for (int i = 0; i < possibleTiles.Count; i++)
        {
            if (CheckForDiamond(possibleTiles[i]))
            {
                if (possibleTiles[i] == current.up)
                {
                    return GameManager.Direction.Up;
                }

                if (possibleTiles[i] == current.down)
                {
                    return GameManager.Direction.Down;

                }

                if (possibleTiles[i] == current.left)
                {
                    return GameManager.Direction.Left;

                }

                if (possibleTiles[i] == current.right)
                {
                    return GameManager.Direction.Right;
                }
            }

            //check for the goal
            if (CheckForGoal(possibleTiles[i]))
            {
                goal = possibleTiles[i];

                //if the player has the diamond, go to the goal
                if(hasDiamond)
                {
                    if (possibleTiles[i] == current.up)
                    {
                        return GameManager.Direction.Up;
                    }

                    if (possibleTiles[i] == current.down)
                    {
                        return GameManager.Direction.Down;

                    }

                    if (possibleTiles[i] == current.left)
                    {
                        return GameManager.Direction.Left;

                    }

                    if (possibleTiles[i] == current.right)
                    {
                        return GameManager.Direction.Right;
                    }
                }
            }

        }

        //go in a random direction
        int index = Random.Range(0, possibleTiles.Count);

        if (possibleTiles[index] == current.up)
        {
            return GameManager.Direction.Up;
        }

        if (possibleTiles[index] == current.down)
        {
            return GameManager.Direction.Down;

        }

        if (possibleTiles[index] == current.left)
        {
            return GameManager.Direction.Left;

        }

        if (possibleTiles[index] == current.right)
        {
            return GameManager.Direction.Right;
        }

        return GameManager.Direction.Current;
    }

    //reurns the direction we should move
    GameManager.Direction DirectionToMove()
    {
        //a list of the current tile and adjacent tiles
        List<MapTile> tilesWithoutBombs = new List<MapTile>();

        tilesWithoutBombs.Add(current);
        tilesWithoutBombs.Add(current.up);
        tilesWithoutBombs.Add(current.down);
        tilesWithoutBombs.Add(current.left);
        tilesWithoutBombs.Add(current.right);

        //list of directons. we only add to this if the tile in the given direction is not obstructed
        List<GameManager.Direction> possibleDirections = new List<GameManager.Direction>();
        //index that we will use to return a random direction
        int index;


        //remove a tile if there is a bomb or wall, or if the tile is offgrid
        for (int i = 0; i < tilesWithoutBombs.Count; i++)
        {
            if (tilesWithoutBombs[i].data == GameManager.SensorData.Bomb || tilesWithoutBombs[i].data == GameManager.SensorData.Wall || tilesWithoutBombs[i].data == GameManager.SensorData.OffGrid)
            {
                tilesWithoutBombs.Remove(tilesWithoutBombs[i]);
            }

            //if the player does not have the diamond, look for it
            if (!hasDiamond)
            {
                if (CheckForDiamond(tilesWithoutBombs[i]))
                {
                    if (tilesWithoutBombs[i] == current.left)
                    {
                        return GameManager.Direction.Left;
                    }

                    if (tilesWithoutBombs[i] == current.right)
                    {
                        return GameManager.Direction.Right;
                    }

                    if (tilesWithoutBombs[i] == current.up)
                    {
                        return GameManager.Direction.Up;
                    }

                    if (tilesWithoutBombs[i] == current.down)
                    {
                        return GameManager.Direction.Down;
                    }
                }
            }

            //otherwise look for the goal
            else
            {
                if (CheckForGoal(tilesWithoutBombs[i]))
                {
                    if (tilesWithoutBombs[i] == current.left)
                    {
                        return GameManager.Direction.Left;
                    }

                    if (tilesWithoutBombs[i] == current.right)
                    {
                        return GameManager.Direction.Right;
                    }

                    if (tilesWithoutBombs[i] == current.up)
                    {
                        return GameManager.Direction.Up;
                    }

                    if (tilesWithoutBombs[i] == current.down)
                    {
                        return GameManager.Direction.Down;
                    }
                }
            }
        }




        //checks which directions are able to be moved in and then we randomly decide
        if (tilesWithoutBombs.Contains(current))
        {
            tilesWithoutBombs.Remove(current);
        }
        if (tilesWithoutBombs.Count > 0)
        {
            for (int i = 0; i < tilesWithoutBombs.Count; i++)
            {
                if (tilesWithoutBombs[i] == current.left)
                {
                    possibleDirections.Add(GameManager.Direction.Left);
                }

                if (tilesWithoutBombs[i] == current.right)
                {
                    possibleDirections.Add(GameManager.Direction.Right);
                }

                if (tilesWithoutBombs[i] == current.up)
                {
                    possibleDirections.Add(GameManager.Direction.Up);
                }

                if (tilesWithoutBombs[i] == current.down)
                {
                    possibleDirections.Add(GameManager.Direction.Down);
                }


            }

            index = Random.Range(0, possibleDirections.Count);

            return possibleDirections[index];
        }

        else
        {
            //if there are no elements in the list
            //return a random direction to move, provided therandom direction is not a wall
            index = Random.Range(0, 4);

            return possibleDirections[index];
        }

    }

    //function to create a new map tile
    MapTile CreateTile(GameManager.SensorData aData, GameManager.Direction aDir)
    {
        //store the current key
        int id = keys;



        //increment the keys
        keys++;

        //return a new maptile with the id and the data
        MapTile temp = new MapTile(id, aData);

        switch (aDir)
        {
            case GameManager.Direction.Up:
                temp.down = current;
                break;

            case GameManager.Direction.Down:
                temp.up = current;
                break;

            case GameManager.Direction.Left:
                temp.right = current;
                break;

            case GameManager.Direction.Right:
                temp.left = current;
                break;

            case GameManager.Direction.Current:
                break;
        }

        return temp;

    }

    //checks for a wall
    bool CheckForWall(MapTile aTile)
    {
        if (aTile.data == GameManager.SensorData.Wall)
        {
            return true;
        }

        return false;
    }
}
